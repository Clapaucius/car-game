﻿using System;
using UnityEngine;


public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private Vector3 offset;

    private void Update()
    {
        cameraTransform.position = transform.position + offset;
    }
}
