﻿using System;
using TMPro;
using UnityEngine;


public class UIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI obstacleCount;
    [SerializeField] private TextMeshProUGUI endGameText;
    [SerializeField] private GameObject endGamePanel;
    
    public static UIManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }
    
    public void UpdateObstacleCountUI(int currentCounter)
    {
        obstacleCount.text = $"{currentCounter}";
    }

    public void ShowWinPanel()
    {
        endGamePanel.SetActive(true);
        endGameText.text = "You Won!";
    }
    
    public void ShowLosePanel()
    {
        endGamePanel.SetActive(true);
        endGameText.text = "You Lost!";
    }
}
