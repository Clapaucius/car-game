﻿using System;
using UnityEngine;

public class DragObject : MonoBehaviour
{
    [SerializeField] private float swipeSpeed;
    
    private Vector3 _mouseOffset;
    private float _mousePositionZ;
    
    private Rigidbody _rigidbody;
    private Camera _mainCamera;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _mainCamera = Camera.main;
    }

    private void OnMouseDown()
    {
        _mousePositionZ = _mainCamera.WorldToScreenPoint(transform.position).z;
        _mouseOffset = transform.position - GetMouseWorldPosition();
    }

    private Vector3 GetMouseWorldPosition()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = _mousePositionZ;
        return _mainCamera.ScreenToWorldPoint(mousePoint);
    }
    
    private void OnMouseDrag()
    {
        _rigidbody.velocity = swipeSpeed * new Vector3(-transform.position.x + GetMouseWorldPosition().x + _mouseOffset.x, 0,
            -transform.position.z + GetMouseWorldPosition().z + _mouseOffset.z).normalized;
    }
}
