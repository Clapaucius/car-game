using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    [SerializeField] private float carSpeed;
    [SerializeField] private float accelerationDuration;
    [SerializeField] private float decelerationDuration;

    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        StartCoroutine(Accelerate());
    }

    public void StartDecelerating()
    {
        StopCoroutine(Accelerate());
        StartCoroutine(Decelerate());
    }

    private IEnumerator Accelerate()
    {
        while (_rigidbody.velocity.magnitude < carSpeed)
        {
            _rigidbody.velocity = Vector3.MoveTowards(_rigidbody.velocity, Vector3.left * carSpeed,
                Time.deltaTime / accelerationDuration * carSpeed);
            yield return null;
        }
    }
    
    private IEnumerator Decelerate()
    {
        while (_rigidbody.velocity.magnitude > 0)
        {
            _rigidbody.velocity = Vector3.MoveTowards(_rigidbody.velocity, Vector3.zero, Time.deltaTime / decelerationDuration * carSpeed);
            yield return null;
        }
    }
}
