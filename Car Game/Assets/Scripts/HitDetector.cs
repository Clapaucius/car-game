﻿using System;
using UnityEngine;


public class HitDetector : MonoBehaviour
{
    [SerializeField] private CarController carController;
    
    [SerializeField] private LayerMask obstacleLayer;
    [SerializeField] private LayerMask finishLayer;
    
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == (int)Mathf.Log(obstacleLayer.value, 2))
        {
            LoseGame();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == (int)Mathf.Log(finishLayer.value, 2))
        {
            WinGame();
        }
    }

    private void WinGame()
    {
        carController.StartDecelerating();
        UIManager.Instance.ShowWinPanel();
    }

    private void LoseGame()
    {
        carController.StartDecelerating();
        UIManager.Instance.ShowLosePanel();
    }
}
