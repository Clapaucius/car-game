﻿using System;
using UnityEngine;


public class ObstacleDestroyer : MonoBehaviour
{
    public static Action OnObstacleDestroy;
    
    [SerializeField] private LayerMask obstacleLayer;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == (int)Mathf.Log(obstacleLayer.value, 2))
        {
            OnObstacleDestroy?.Invoke();
            Destroy(other.gameObject, 3f);
        }
    }
}
