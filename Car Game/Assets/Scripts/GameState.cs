﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameState : MonoBehaviour
{
    public int ObstacleCounter { get; private set; }

    private void Start()
    {
        ObstacleDestroyer.OnObstacleDestroy += UpdateObstacleCounter;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1);
    }

    private void UpdateObstacleCounter()
    {
        ObstacleCounter++;
        UIManager.Instance.UpdateObstacleCountUI(ObstacleCounter);
    }
}
